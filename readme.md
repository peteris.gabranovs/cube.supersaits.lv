<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Laravel framework

```
composer create-project --prefer-dist laravel/laravel .
```

* configure "config/database.php" charsets from mb4 to mb2
* configure ".env" database and email credentials

## AdminLTE design

```
composer require jeroennoten/laravel-adminlte
php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=assets
php artisan make:adminlte
php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=config
php artisan migrate
```

## laravel built-in email verification

* edit "app/User.php"
```
class User extends Authenticatable implements MustVerifyEmail
```

* edit "routes/web.php"
```
Auth::routes(['verify' => true]); // enable laravel email verification
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified'); // add midleware to check verified email
```

## Add ajax to check unique email

* add method to "app/Http/Controllers/Auth/RegisterController.php"

```
    /**
     * Check for unique email
     *
     * @param Request $request
     * @return JsonResponse
     */
    protected function checkEmailUnique(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
        ]);

        if ($validator->fails()) {
            return new JsonResponse([
                "status"  => "error",
                "message" => $validator->errors()->first('email'),
            ]);
        }
        return new JsonResponse([
            "status"  => "success",
            "message" => "",
        ]);
    }
```
	
* add route to "routes/web.php"

```
	Route::post('/check_email_unique', 'Auth\RegisterController@checkEmailUnique')->name('check_email_unique');
```

* add ajax call on form submit in "resources/views/auth/register.blade.php"

```
    @section('js')
        @parent
        <script type="text/javascript">
            // clear old errors
            $(".register-box form input").on('keyup keypress blur change',function () {
                $(this).parent('.form-group').removeClass('has-error').find('.help-block').remove();
            });
            // check unique email (this can be extended for all validations)
            $(".register-box form").submit(function () {
                if ($(this).find("[name=email]").val()) {
                    $.post('{{ url('check_email_unique') }}', {'_token': '{{ csrf_token() }}', 'email': $(this).find('[name=email]').val()}, function (response) {
                    if (response.status === 'success') {
                        $($('.register-box form')).unbind('submit').submit();
                    } else {
                        $('.form-group').has('[name=email]').addClass('has-error').append('<span class="help-block"><strong>' + response.message + '</strong></span>');
                    }
                });
                    return false;
                }
            });
        </script>
    @stop
```

## Get feeds

```
composer require willvincent/feeds "dev-master"
php artisan vendor:publish --provider="willvincent\Feeds\FeedsServiceProvider"
```

* in controller "app\Http\Controllers\HomeController.php"

```
    public function index()
    {
        $feed = \Feeds::make('http://www.delfi.lv/rss/?channel=delfi');
        $data = [
            'title'     => $feed->get_title(),
            'permalink' => $feed->get_permalink(),
            'items'     => $feed->get_items(),
        ];
        return view('home', [
            "feeds" => $data,
        ]);
    }
```

* in "resources/views/home.blade.php"

```
    <div class="panel panel-primary">
        <div class="panel-heading">{{ $feeds["title"] }}</div>
        <div class="panel-body">
            <ul class="list-group">
                @foreach($feeds["items"] as $item)
                    <li class="list-group-item">
                        <h2><a href="{{ $item->get_permalink() }}" target="_blank">{{ $item->get_title() }}</a></h2>
                        <p>{{ $item->get_description() }}</p>
                        <p><small><i>Posted on {{ $item->get_date('j F Y | g:i a') }}</i></small></p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
```
	


