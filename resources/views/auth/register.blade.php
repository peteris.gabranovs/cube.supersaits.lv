@extends('adminlte::register')

@section('js')
    @parent
    <script type="text/javascript">
        // clear old errors
        $(".register-box form input").on('keyup keypress blur change',function () {
            $(this).parent('.form-group').removeClass('has-error').find('.help-block').remove();
        });
        // check unique email (this can be extended for all validations)
        $(".register-box form").submit(function () {
            if ($(this).find("[name=email]").val()) {
                $.post('{{ url('check_email_unique') }}', {'_token': '{{ csrf_token() }}', 'email': $(this).find('[name=email]').val()}, function (response) {
                    if (response.status === 'success') {
                        $($('.register-box form')).unbind('submit').submit();
                    } else {
                        $('.form-group').has('[name=email]').addClass('has-error').append('<span class="help-block"><strong>' + response.message + '</strong></span>');
                    }
                });
                return false;
            }
        });
    </script>
@stop