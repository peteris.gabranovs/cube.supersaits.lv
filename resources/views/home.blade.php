@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>You are logged in!</p>

    <div class="panel panel-primary">
        <div class="panel-heading">{{ $feeds["title"] }}</div>
        <div class="panel-body">
            <ul class="list-group">
                @foreach($feeds["items"] as $item)
                    <li class="list-group-item">
                        <h2><a href="{{ $item->get_permalink() }}" target="_blank">{{ $item->get_title() }}</a></h2>
                        <p>{{ $item->get_description() }}</p>
                        <p><small><i>Posted on {{ $item->get_date('j F Y | g:i a') }}</i></small></p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@stop